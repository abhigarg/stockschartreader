# -*- coding: utf-8 -*-
"""
Created on Mon Dec 22 14:13:29 2014

@author: abgg
"""

import cv2
import numpy as np
import cv2.cv  as cv
import math

##### module for locating colored lines ##########

def detectLines(img, background):

    cimg = np.zeros(img.shape, dtype = 'uint8');        
    
    cimg[np.where((cimg == [0,0,0]).all(axis = 2))] = [255,255,255]


    # to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # threshold
    #ret1, thresh1 = cv2.threshold(gray, 170, 255, cv2.THRESH_BINARY) # works for white background
    #ret1, thresh1 = cv2.threshold(gray, 70, 255, cv2.THRESH_BINARY_INV) # for black background  
    
    if(background == 0):
        ret1, thresh1 = cv2.threshold(gray, 70, 255, cv2.THRESH_BINARY)
        thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,251,0)
    else:
        ret1, thresh1 = cv2.threshold(gray, 170, 255, cv2.THRESH_BINARY_INV)
        thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV,251,0)

    cv2.imshow('thresh1', thresh1)
    #cv2.imshow('thresh', thresh)

    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

    cv2.imshow('opening', opening)

    chart = cv2.bitwise_and(opening, thresh1)
    cv2.imshow('chart', chart)
    
    # contours
    (cnts, _) = cv2.findContours(chart.copy(),cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

    # loop over our contours
    for c in cnts:        
        cnt_area = cv2.contourArea(c)
        if (cnt_area < 10000):
            # approximation
            epsilon = 0.02*cv2.arcLength(c,True)
            approx = cv2.approxPolyDP(c,epsilon,True)

            pts = np.array((4,2), dtype = 'float32')
            pts = np.vstack(approx).squeeze()
        
            if (len(approx) == 4):
                #cv2.drawContours(cimg, approx, -1, (0, 255, 0), 2) 
                srcPts = np.zeros((4, 2), dtype = "float32")

                s = pts.sum(axis = 1)
                srcPts[0] = pts[np.argmin(s)]
                srcPts[2] = pts[np.argmax(s)]
            
                diff = np.diff(pts, axis = 1)
                srcPts[1] = pts[np.argmin(diff)]
                srcPts[3] = pts[np.argmax(diff)]
            
                (tl, tr, br, bl) = srcPts

                cv2.line(cimg, (tl[0], tl[1]), (tr[0], tr[1]), (255, 0, 0), 1)
                cv2.line(cimg, (bl[0], bl[1]), (br[0], br[1]), (255, 0, 0), 1)
                cv2.line(cimg, (tl[0], tl[1]), (bl[0], bl[1]), (255, 0, 0), 1)
                cv2.line(cimg, (tr[0], tr[1]), (br[0], br[1]), (255, 0, 0), 1)
                
                centroid = int((tl[1]+bl[1])/2)

                cv2.line(cimg, (tl[0], centroid), (br[0], centroid), (0, 0, 0), 2)                

    cv2.imshow('contours', cimg)


##### main function ######

##fname = "C:\Users\abgg\Downloads\odesk\New\stock_chart_tracker\chartvideo6.mpg"
##
##splitfname = fname.split('.',1)

cap = cv2.VideoCapture("chartvideo3.mp4")

ret, frame = cap.read()

(h,w,c) = frame.shape


print frame.shape


# detect background

black = cv2.inRange(frame, (0,0,0),(30,30,30))
white = cv2.inRange(frame, (245,245,245), (255, 255, 255))

nBlack = cv2.countNonZero(black)
nWhite = cv2.countNonZero(white)

if(nBlack > nWhite):
    print "background is black"
    background = 0
    #cv2.imshow('black', black)
else:
    print "background is white"
    background = 1
    #cv2.imshow('white', white)

if(cv2.waitKey(1) & 0xFF == ord('q')):
    exit


frame_count = 0


while (True):
    ret, frame = cap.read()

    frame_count += 1
    #print frame_count
    
    if ret is not True:
        break

    cv2.imshow('stocks', frame)


    detectLines(frame, background)
    
    
    if(cv2.waitKey(1) & 0xFF == ord('q')):
       break

    

cap.release()
cv2.destroyAllWindows()
print frame_count
