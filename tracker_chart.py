# -*- coding: utf-8 -*-

import cv2
import numpy as np
import cv2.cv  as cv
import math
#import xlwt

##### module for locating colored lines ##########

def detectLines(img, background, bars):

    cimg = np.zeros(img.shape, dtype = 'uint8');

    (h,w,c) = img.shape
    
    cimg[np.where((cimg == [0,0,0]).all(axis = 2))] = [255,255,255]


    # to grayscale
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)

    # threshold
    #ret1, thresh1 = cv2.threshold(gray, 170, 255, cv2.THRESH_BINARY) # works for white background
    #ret1, thresh1 = cv2.threshold(gray, 70, 255, cv2.THRESH_BINARY_INV) # for black background  
    
    if(background == 0):
        ret1, thresh1 = cv2.threshold(gray, 90, 255, cv2.THRESH_BINARY)
        thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY,251,0)
    else:
        ret1, thresh1 = cv2.threshold(gray, 90, 255, cv2.THRESH_BINARY_INV)
        thresh = cv2.adaptiveThreshold(gray,255,cv2.ADAPTIVE_THRESH_MEAN_C, cv2.THRESH_BINARY_INV,251,0)

    kernel = np.ones((3,15),np.uint8)
    closing = cv2.morphologyEx(thresh1, cv2.MORPH_CLOSE, kernel)
    #cv2.imshow('closing', closing)
    temp = cv2.subtract(thresh1,closing)
    
    #thresh1 = cv2.dilate(thresh1,kernel,iterations = 1)

    lines = cv2.HoughLinesP(thresh1,1,np.pi/180,1,1,1)
    chart_lines = []
    for x1,y1,x2,y2 in lines[0]:
        if(x1 == x2):
            chart_lines.append([x1,y1,x2,y2,int((x1+x2)/2), int((y1+y2)/2)])
            cv2.line(cimg,(x1,y1),(x2,y2),(255,255,0),1)

##    cv2.imshow('thresh1', thresh1)
##    cv2.imshow('thresh', thresh)

    kernel = np.ones((3,3),np.uint8)
    opening = cv2.morphologyEx(thresh, cv2.MORPH_OPEN, kernel)

    #cv2.imshow('opening', opening)

    chart = cv2.bitwise_and(opening, thresh1)
    #cv2.imshow('chart', chart)

    if(bars):

        bars = []
        bars_lines = []
        bars_center = []  # center y of thick bar
        centroid_x = []
        centroid_y = []   # center y of big tick
        
        # contours
        (cnts, _) = cv2.findContours(thresh1.copy(),cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

        # loop over our contours
        for c in cnts:        
            cnt_area = cv2.contourArea(c)
            if (cnt_area < 0.5*h*w):
                # approximation
                epsilon = 0.005*cv2.arcLength(c,True)
                approx = cv2.approxPolyDP(c,epsilon,True)

                #pts = np.array((4,2), dtype = 'float32')
                #pts = np.vstack(approx).squeeze()

                # centroid of box
                M = cv2.moments(c)
                if(M['m00']==0):
                    print('moment m00 is zero')
                    continue
                
                centroid_x.append(int(M['m10']/M['m00']))
                bars_center.append(int(M['m01']/M['m00']))    #center of bar

                # bounding boxes rotated
                rect = cv2.minAreaRect(c)
                box = cv.BoxPoints(rect)
                box = np.int0(box)
                bars.append(box)
                centroid_y.append(int((box[0][1]+box[1][1]+box[2][1]+box[3][1])/4))    #center of thick bar
                cv2.drawContours(cimg,[box],0,(255,0,255),1)

                #lines contained in box
                collection_lines = []
                for i in range(0, len(chart_lines)-1):
                    x = chart_lines[i][4]
                    y = chart_lines[i][5]
                    if(cv2.pointPolygonTest(c, (x,y), False)):
                        collection_lines.append(chart_lines[i])

                bars_lines.append(collection_lines) 
                

        # sort bars and lines in order of x values
        n = len(centroid_x)
        cent_x = np.array((1,n), dtype = 'int32')
        cent_x = np.hstack([centroid_x]).squeeze()
        sort_ind = np.argsort(cent_x,axis=0)
        print np.sort(cent_x)
        print cent_x[sort_ind[0]]
        print cent_x[sort_ind[1]]
        bars_sorted = []
        bars_lines_sorted = []
        bars_centroid = []
        bars_lines_centroid = []
        bars_centroid_y = []
        bars_centroid_x = []

        for i in sort_ind:
            bar_sorted = bars[sort_ind[i]]
            bar_line_sorted = bars_lines[sort_ind[i]]
            bar_centroid = bars_center[sort_ind[i]]
            bars_sorted.append(bar_sorted)
            bars_lines_sorted.append(bar_line_sorted)
            bars_centroid.append(bar_centroid)
            bars_centroid_x.append(centroid_x[sort_ind[i]])
            bars_centroid_y.append(centroid_y[sort_ind[i]])
            cv2.circle(cimg, (centroid_x[sort_ind[i]],int(bar_centroid)), 2,(0,0,255),1)
            cv2.circle(cimg, (centroid_x[sort_ind[i]],centroid_y[sort_ind[i]]), 2,(255,0,0),1)
            cv2.imshow('contours', cimg)
            if(cv2.waitKey(0) & 0xFF == ord('q')):
                exit

            #for j in bar_line_sorted:
                        
    cv2.imshow('contours', cimg)

    return (bars_centroid, bars_centroid_x, bars_centroid_y) 

##### main function ######

##fname = "C:\Users\abgg\Downloads\odesk\New\stock_chart_tracker\chartvideo6.mpg"
##
##splitfname = fname.split('.',1)

#prepare output excel file
##book = xlwt.Workbook(encoding="utf-8")
##sheet1 = book.add_sheet("Sheet 1")                           
##sheet1.write(0, 0, "Time")
##sheet1.write(1, 0, "Stock value")                               

#video capture
cap = cv2.VideoCapture("applesolid.mpg")

bars = 1

# capture first frame
ret, frame = cap.read()
(h,w,c) = frame.shape

# detect background

black = cv2.inRange(frame, (0,0,0),(30,30,30))
white = cv2.inRange(frame, (245,245,245), (255, 255, 255))

nBlack = cv2.countNonZero(black)
nWhite = cv2.countNonZero(white)

if(nBlack > nWhite):
    print ("background is black")
    background = 0
    #cv2.imshow('black', black)
else:
    print ("background is white")
    background = 1
    #cv2.imshow('white', white)

(bars_centroid, bars_centroid_x, bars_centroid_y) = detectLines(frame, background, bars)

print len(bars_centroid)

numbars = len(bars_centroid_x)

print "last column x center: "
print bars_centroid_x[numbars-1]

x1 = (bars_centroid_x[numbars-2] + bars_centroid_x[numbars-1])/2

x2 = 2*bars_centroid_x[numbars-1]-x1

# crop last bar

#last_bar = frame[0:h, x1:x2]

cv2.rectangle(frame,(x1,0),(x2,h),(0,255,0),2)

cv2.imshow('last', frame)


if(cv2.waitKey(0) & 0xFF == ord('q')):
    exit



frame_count = 0


# track most recent stock value
while (True):
    ret, frame = cap.read()

    frame_count += 1
    #print frame_count
    
    if ret is not True:
        break

    cv2.imshow('stocks', frame)


    #detectLines(frame, background, bars)
    
    
    if(cv2.waitKey(1) & 0xFF == ord('q')):
       break

    

cap.release()
cv2.destroyAllWindows()
print (frame_count)
